package jeq.playground.projecttwice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {

    @GetMapping("/test")
    public String test() {
        return "test.html";
    }

    @GetMapping("/main")
    public String main(Model model, @RequestParam(name = "module", required = false, defaultValue = "dashboard") String module) {
        System.out.println("dashboard");
        switch (module) {
            case "dashboard" -> model.addAttribute("module", "dashboard.html");
            case "products" -> {
                System.out.println("products");
                model.addAttribute("module", "product.html");
            }
            default -> model.addAttribute("module", "home.html");
        }

        return "main.html";
    }
}
