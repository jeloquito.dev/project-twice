package jeq.playground.projecttwice.controller;

import jeq.playground.projecttwice.dto.LoginDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

@Controller
public class AuthController {

    @GetMapping("/login")
    public String displayLogin(Model model) {

        RestTemplate restTemplate = new RestTemplate();
        String fooResourceUrl
                = "http://localhost:8080/auth/login";
        ResponseEntity response = restTemplate.getForEntity(fooResourceUrl, String.class);

        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setUsername(response.getBody().toString());
        model.addAttribute("login", loginDTO);
        return "login";
    }

    @PostMapping("/login")
    public String processLogin(@ModelAttribute("login") LoginDTO loginDTO) {

        System.out.println("Login DTO: " + loginDTO.getUsername() + ", " + loginDTO.getPassword());
        return "login";
    }
}
