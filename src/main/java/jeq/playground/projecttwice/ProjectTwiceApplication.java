package jeq.playground.projecttwice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectTwiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectTwiceApplication.class, args);
	}

}
