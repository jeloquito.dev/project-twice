package jeq.playground.projecttwice.service;

import jeq.playground.projecttwice.dto.LoginDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class LoginService {

    public void something() {

        RestTemplate restTemplate = new RestTemplate();
        String fooResourceUrl
                = "http://localhost:8080/auth/login";
        ResponseEntity response = restTemplate.getForEntity(fooResourceUrl, String.class);

        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setUsername(response.getBody().toString());
    }
}
